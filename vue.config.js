module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/dgm-board' : '',
    outputDir: 'example',
    chainWebpack: config => {
        config.plugins.delete('prefetch')

        // Preserve white spaces for ssh-pre component.
        config.module
            .rule('vue')
            .use('vue-loader')
            .loader('vue-loader')
            .tap(options => {
                options.compilerOptions.whitespace = 'preserve'
                return options
            })
    },
    productionSourceMap: false,
    lintOnSave: true,
}
