import List from "../list/List";
import Thumbnail from "../list/Thumbnail";
import Pagination from "../page/Pagination";
import Lazyload from "../page/Lazyload";

export default class TemplateUtils {
    listTemp = '';
    pageTemp = '';
    listComponent = '';
    pageComponent = '';

    defineListTemp = {
        list: {component: List},
        thumbnail: {component: Thumbnail},
    };
    definePageTemp = {
        'pagination': {component: Pagination, type: 'pagination'},
        'lazyload' : {component: Lazyload, type: 'lazyload'},
    };

    constructor() {
        this.listTemp = 'list';
        this.pageTemp = 'page';
        this.listComponent = List;
        this.pageComponent = Pagination;
        this.paginationType = 'pagination';
    }

    setViewTemp(listTemp, pageTemp) {
        this.listTemp = this.checkListTemp(listTemp) ? listTemp : 'list';
        this.pageTemp = this.checkPageTemp(pageTemp) ? pageTemp : 'pagination';
        this.listComponent = this.defineListTemp[this.listTemp].component;
        this.pageComponent = this.definePageTemp[this.pageTemp].component;
        let paginationType = this.definePageTemp[this.pageTemp].type;
        return {
            listTemp: this.listTemp,
            pageTemp: this.pageTemp,
            listComponent: this.listComponent,
            pageComponent: this.pageComponent,
            paginationType: paginationType,
        }
    }

    checkListTemp(listTemp) {
        let defineTypes = Object.keys(this.defineListTemp);
        return defineTypes.indexOf(listTemp) > -1;
    }

    checkPageTemp(pageTemp) {
        let defineTypes = Object.keys(this.definePageTemp);
        return defineTypes.indexOf(pageTemp);
    }

}
