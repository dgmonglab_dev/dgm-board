export default class DataUtils {
    allDatas = [];
    pageData = {
        page: 1,
        size: 10,
        total: 0,
    };
    datas = [];
    paginationType = '';

    constructor() {
        this.paginationType = 'pagination';
        this.pageData = { page: 1, size: 10, total: 0 };
    }

    initData() {
        this.pageData = { page: 1, size: 10, total: 0 };
        this.datas = [];
        this.allDatas = [];
    }

    isArray(data) {
        return Array.isArray(data);
    }

    setPaginationType(paginationType) {
        this.paginationType = paginationType;
        return this.makeDatas();
    }

    getPaginationType() {
        return this.paginationType;
    }

    setPage(page) {
        this.pageData.page = page;
        return this.makeDatas();
    }

    setAllDatas(allDatas) {
        if(this.isArray(allDatas)) {
            this.initData();
            this.allDatas = allDatas;
            this.pageData.total = this.allDatas.length;
        }
        return this.makeDatas();
    }

    makeDatas() {
        this.datas = this.paginationType === 'pagination' ? this.makePaginationData() : this.makeLazyloadData();
        return {
            pageData: this.pageData,
            datas: this.datas,
            allDatas: this.allDatas,
        }
    }

    makePaginationData() {
        let startIndex = (this.pageData.page - 1) * this.pageData.size;
        let endIndex = this.pageData.page * this.pageData.size;
        let data = [];
        data = this.allDatas.slice(startIndex, endIndex);
        return data;
    }

    makeLazyloadData() {
        let endIndex = this.pageData.page * this.pageData.size;
        let data = [];
        if(this.allDatas.length > endIndex) {
            data = this.allDatas.slice(0, endIndex);
        }else{
            data = this.allDatas.slice();
        }
        return data;
    }
}
