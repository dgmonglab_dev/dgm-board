import Vue from 'vue'
import VueRouter from 'vue-router'
import Example from "../example/Example";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Example
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
